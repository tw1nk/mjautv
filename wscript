
def options(opt): 
    opt.load('compiler_c')
    opt.add_option('--debug', action='store_true', default=False, dest='debug', help='Debug mode')
    
def configure(conf):
    conf.load('compiler_c')
    conf.check_cfg(package="webkitgtk-3.0", uselib_store = 'WEBKITGTK3', args ="--cflags --libs")
    conf.check_cfg(package="gtk+-3.0", uselib_store = 'GTK3', args = '--cflags --libs')
    conf.check_cfg(package="libnotify", uselib_store = 'LIBNOTIFY', args = '--cflags --libs')
    conf.check_cfg(package="dbus-glib-1", uselib_store = "DBUS", args = '--cflags --libs')
    # set 'default' variant
    conf.define ('DEBUG', 0)
    conf.env['CFLAGS']=['-O2']
    
    # set some debug relevant config values
    if conf.options.debug:
        conf.define ('DEBUG', 1)
        conf.env['CFLAGS'] = ['-O0', '-g3']
        
def build(bld):
    bld.recurse('src')