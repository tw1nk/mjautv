#include <stdio.h>
#include <signal.h>

#include <gtk/gtk.h>
#include <gdk/gdkkeysyms.h>
#include <webkit/webkit.h>
#include "jsext/customObject.h"
#include "jsext/notification.h"
#include "jsext/system.h"
#include "jsext/system_battery.h"

/*
* Callback - Javascript window object has been cleared 
*/
	
static void window_object_cleared_cb(WebKitWebView *web_view,
	WebKitWebFrame *frame,
	gpointer	context,
	gpointer	window_object,
	gpointer	user_data) {
	
		g_message("window object cleared cb");
		/** Add classes to JavaScriptCore */
		JSClassRef classDef = JSClassCreate(&class_def);
		JSObjectRef classObj = JSObjectMake(context, classDef, context);
		JSObjectRef globalObj = JSContextGetGlobalObject(context);
		JSStringRef str = JSStringCreateWithUTF8CString("CustomClass");
		JSObjectSetProperty(context, globalObj, str, classObj, kJSPropertyAttributeNone, NULL);
		
		g_message("connecting notification");
		classDef = JSClassCreate(&notification_def);
		classObj = JSObjectMake(context, classDef, context);
		str = JSStringCreateWithUTF8CString("Notification");
		JSObjectSetProperty(context, globalObj, str, classObj, kJSPropertyAttributeNone, NULL);
		
		g_message("creating system");
		classDef = JSClassCreate(&system_def);
		classObj = JSObjectMake(context, classDef, context);
		str = JSStringCreateWithUTF8CString("System");
		JSObjectSetProperty(context, globalObj, str, classObj, kJSPropertyAttributeNone, NULL);
		
		g_message("create system.battery");
		
		system_battery_def.parentClass = classDef;
		JSClassRef batteryClassDef = JSClassCreate(&system_battery_def);
		JSObjectRef batteryClassObj = JSObjectMake(context, batteryClassDef, context);
		str = JSStringCreateWithUTF8CString("Battery");
		JSObjectSetProperty(context, classObj, str, batteryClassObj, kJSPropertyAttributeNone, NULL);	
}

static void destroy(GtkWidget * widget, gpointer data) {
	gtk_main_quit();
}


gboolean on_key_press(GtkWidget*, GdkEventKey*, gpointer);

void reload_browser(int);
void toggle_fullscreen(int);
void maximize();
void unmaximize();

static WebKitWebView* web_view;
static GtkWidget *window;
gchar* default_url = "file:///home/callegustafsson/src/mjautv/testhtml/customobject.html";

#ifndef GDK_KEY_F5
#define GDK_KEY_F5 GDK_F5
#endif

#ifndef GDK_KEY_F11
#define GDK_KEY_F11 GDK_F11
#endif

int main(int argc, char** argv) {
  gtk_init(&argc, &argv);

  window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
  
  g_signal_connect(window, "key-press-event", G_CALLBACK(on_key_press), NULL);
 
  /* Connect the destroy window event with destroy function */
  g_signal_connect(G_OBJECT (window), "destroy", G_CALLBACK (destroy), NULL);


  web_view = WEBKIT_WEB_VIEW(webkit_web_view_new());

  signal(SIGHUP, reload_browser);
  signal(SIGUSR1, toggle_fullscreen);
  
  /* Connect the window object cleared event with callback */
  g_signal_connect (G_OBJECT (web_view), "window-object-cleared", G_CALLBACK(window_object_cleared_cb), web_view);

  gtk_container_add(GTK_CONTAINER(window), GTK_WIDGET(web_view));

  if(argc > 1) {
    webkit_web_view_load_uri(web_view, argv[1]);
  }
  else {
    webkit_web_view_load_uri(web_view, default_url);
  }
  gtk_window_resize(GTK_WINDOW(window), 640, 480);
  //maximize();
  gtk_widget_show_all(window);
  gtk_main();

  return 0;
}

gboolean on_key_press(GtkWidget* window, GdkEventKey* key, gpointer userdata) {
  if(key->type == GDK_KEY_PRESS && key->keyval == GDK_KEY_F5) {
    reload_browser(0);
  }
  else if(key->type == GDK_KEY_PRESS && key->keyval == GDK_KEY_F11) {
    toggle_fullscreen(0);
  }

  return FALSE;
}

void reload_browser(int signum) {
  webkit_web_view_reload_bypass_cache(web_view);
}

void toggle_fullscreen(int signum) {
  if(gtk_window_get_decorated(GTK_WINDOW(window))) {
    maximize();
  } else {
    unmaximize();
  }
}

void maximize() {
  gtk_window_maximize(GTK_WINDOW(window));
  gtk_window_fullscreen(GTK_WINDOW(window));
  gtk_window_set_decorated(GTK_WINDOW(window), FALSE);
}

void unmaximize() {
  gtk_window_unmaximize(GTK_WINDOW(window));
  gtk_window_unfullscreen(GTK_WINDOW(window));
  gtk_window_set_decorated(GTK_WINDOW(window), TRUE);
  gtk_window_resize(GTK_WINDOW(window), 1280, 768);
}
