#include <gtk/gtk.h>
#include <JavaScriptCore/JavaScript.h>
#include "customObject.h"

/**
* Class initializion
*/

void class_init_cb(JSContextRef ctx, JSObjectRef object) {
	g_message("Custom class init");
}

void class_finalize_cb(JSObjectRef object) {
	g_message("Custom class finalize.");
}

/* Class constructor. Called at "new CustomClass()" */
JSObjectRef class_constructor_cb(JSContextRef context,
                                 JSObjectRef constructor,
                                 size_t argumentCount,
                                 const JSValueRef arguments[],
                                 JSValueRef* exception) {

    g_message("Custom class constructor");
	
    if (argumentCount > 0 && JSValueIsStrictEqual(context, arguments[0], JSValueMakeNumber(context, 0)))
         return JSValueToObject(context, JSValueMakeNumber(context, 1), exception);
    
     return JSValueToObject(context, JSValueMakeNumber(context, 0), exception);
}

const JSClassDefinition class_def = {
    0,                     // version
    kJSClassAttributeNone, // attributes
    "CustomClass",         // className
    NULL,                  // parentClass
    NULL,                  // staticValues
    NULL,                  // staticFunctions
    class_init_cb,         // initialize
    class_finalize_cb,     // finalize
    NULL,                  // hasProperty
    NULL,                  // getProperty
    NULL,                  // setProperty
    NULL,                  // deleteProperty
    NULL,                  // getPropertyNames
    NULL,                  // callAsFunction
    class_constructor_cb,  // callAsConstructor
    NULL,                  // hasInstance  
    NULL                   // convertToType
};