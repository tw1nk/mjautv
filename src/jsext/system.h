#ifndef _MJAUTV_JSEXT_SYSTEM_H_
#define _MJAUTV_JSEXT_SYSTEM_H_

static const JSClassDefinition system_def =
{
    0,                     // version
    kJSClassAttributeNone, // attributes
    "System",              // className
    NULL,                  // parentClass
    NULL,                  // staticValues
    NULL,   			   // staticFunctions
    NULL,			       // initialize
    NULL,    			   // finalize
    NULL,                  // hasProperty
    NULL,                  // getProperty
    NULL,                  // setProperty
    NULL,                  // deleteProperty
    NULL,                  // getPropertyNames
    NULL,                  // callAsFunction
    NULL,                  // callAsConstructor
    NULL,                  // hasInstance  
    NULL                   // convertToType
};


#endif