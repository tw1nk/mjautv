#include <gtk/gtk.h>
#include <JavaScriptCore/JavaScript.h>
#include <libnotify/notify.h>

#include "notification.h"

static void notification_init_cb(JSContextRef ctx, JSObjectRef object) {
	
	notify_init("Notification");
}

static JSValueRef notification_notify_cb(JSContextRef context,
	JSObjectRef function,
	JSObjectRef thisObject,
	size_t argumentCount,
	const JSValueRef arguments[],
	JSValueRef *exception) {
		
		if (argumentCount > 0 && JSValueIsString(context, arguments[0])) {
			
			int timeout = 3000;
			
			if (argumentCount == 2 && JSValueIsNumber(context, arguments[1])) {
				timeout = (int) JSValueToNumber(context, arguments[1], NULL);
				g_message("Timeout set from JS: %i", timeout);
			}
			/* convert jsvalue to char */
			size_t len;
			char *cstr;
			JSStringRef jsstr = JSValueToStringCopy(context, arguments[0], NULL);
			len = JSStringGetMaximumUTF8CStringSize(jsstr);
			cstr = g_new(char, len);
			JSStringGetUTF8CString(jsstr, cstr, len);
			
			/* Create new notifynotification */
			NotifyNotification *notification = notify_notification_new(cstr, NULL, NULL);
			
			/* set timeout*/
			notify_notification_set_timeout(notification, timeout);
			
			notify_notification_set_urgency(notification, NOTIFY_URGENCY_NORMAL);
			
			GError *error = NULL;
			
			notify_notification_show(notification, &error);
			
			g_object_unref(G_OBJECT(notification));
			g_free(cstr);
			JSStringRelease(jsstr);	
		}
		
		return JSValueMakeUndefined(context);
}

static const JSStaticFunction notification_staticfuncs[] = {
	{"notify", notification_notify_cb, kJSPropertyAttributeReadOnly},
	{NULL, NULL, 0}	
};

const JSClassDefinition notification_def = {
   	  0,                     // version
      kJSClassAttributeNone, // attributes
      "Notification",        // className
      NULL,                  // parentClass
      NULL,                  // staticValues
      notification_staticfuncs, // staticFunctions
      notification_init_cb,  // initialize
      NULL,                  // finalize
      NULL,                  // hasProperty
      NULL,                  // getProperty
      NULL,                  // setProperty
      NULL,                  // deleteProperty
      NULL,                  // getPropertyNames
      NULL,                  // callAsFunction
      NULL,                  // callAsConstructor
      NULL,                  // hasInstance  
      NULL                   // convertToType
};
