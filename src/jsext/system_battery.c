#include <dbus/dbus.h>
#include <dbus/dbus-glib.h>
#include <gtk/gtk.h>
#include <JavaScriptCore/JavaScript.h>

#include "system_battery.h"

DBusGConnection *conn;
DBusGProxy *proxy;
DBusGProxy *properties_proxy;

static void system_battery_init_cb(JSContextRef ctx, JSObjectRef object) {
	g_message("system_battery_init_cb");
	GError *error = NULL;
	/** connect to the system bus */
	
	conn = dbus_g_bus_get(DBUS_BUS_SYSTEM, &error);
	
	if (conn == NULL) {
		g_printerr("Failed to open connection to bus: %s\n", error->message);
		
		g_error_free(error);
		return;
	}
	
	proxy = dbus_g_proxy_new_for_name(conn,
		"org.freedesktop.UPower",
		"/org/freedesktop/UPower/devices/battery_BAT0",
		"org.freedesktop.UPower.Device.Properties");
	
	if (proxy == NULL) {
		g_printerr("Failed to create proxy object\n");
		return;
	}
	
	properties_proxy = dbus_g_proxy_new_from_proxy(proxy,
		"org.freedesktop.DBus.Properties",
		dbus_g_proxy_get_path(proxy));
	
	if (properties_proxy == NULL) {
		g_object_unref(proxy);
		g_printerr("failed to create properties proxy object\n");
		return;
	}
	
	error = NULL;
}

/**
* Battery finalize.
*/
static void system_battery_destroy_cb(JSObjectRef object) {
	g_message("system battery destroy cb");

	if (proxy != NULL) {
		g_object_unref(proxy);
	}

	if (properties_proxy != NULL) {
		g_object_unref(properties_proxy);
	}
	
}

static gboolean proxy_property_value(char *property, GValue *get_value, GError **error) {
	g_message("proxy property value");
	if (properties_proxy == NULL) {
		g_printerr("Error: properties proxy is NULL FAILLLL\n");
	}
	
	return dbus_g_proxy_call(properties_proxy, "Get", error, 
		G_TYPE_STRING, "/org/freedesktop/UPower/devices/battery_BAT0",
		G_TYPE_STRING, property,
		G_TYPE_INVALID,
		G_TYPE_VALUE, get_value,
		G_TYPE_INVALID);
}

static JSValueRef proxy_double_value(JSContextRef context, 
	char *property, 
	size_t argumentCount) {
		
		g_message("proxy double value");
		/* call to uPower to get double value */
		GError *error = NULL;
		GValue get_value = {0, };
		if (argumentCount == 0) {
			if(!proxy_property_value(property, &get_value, &error)) {
				g_printerr("Error: %s\n", error->message);
				g_message("ERROR");
				g_error_free(error);
				return JSValueMakeUndefined(context);
			}
			
			gdouble value = g_value_get_double(&get_value);
			g_value_unset(&get_value);
			g_message("YEAH %d", value);
			return JSValueMakeNumber(context, value);
		}
		g_message("WTF");
	return JSValueMakeUndefined(context);	
}

static JSValueRef proxy_uint64_value(JSContextRef context,
	char *property,
	size_t argumentCount) {
		g_message("proxy_uint64_value");
		GError *error = NULL;
		GValue get_value = {0,};
		
		if (argumentCount == 0) {
			if (!proxy_property_value(property, &get_value, &error)) {
				g_printerr("Error: %s\n", error->message);
				g_error_free(error);
				return JSValueMakeUndefined(context);
			}
			guint64 value = g_value_get_uint64(&get_value);
			g_value_unset(&get_value);
			return JSValueMakeNumber(context, value);
		}
		
	return JSValueMakeUndefined(context);	
}


static JSValueRef proxy_boolean_value(JSContextRef context, 
	char *property,
	size_t argumentCount) {
		g_message("proxy_boolean_value");
		GError *error = NULL;
		GValue get_value = {0,};
		
		if (argumentCount == 0) {
			if(!proxy_property_value(property, &get_value, &error)) {
				g_printerr("Error %s\n", error->message);
				g_error_free(error);
				return JSValueMakeUndefined(context);
			}
			
			gboolean value = g_value_get_boolean(&get_value);
			g_value_unset(&get_value);
			
			return JSValueMakeBoolean(context, value);
		}
			
		return JSValueMakeUndefined(context);
}

static JSValueRef battery_capacity_cb(JSContextRef context,
	JSObjectRef function,
	JSObjectRef thisObject,
	size_t argumentCount,
	const JSValueRef arguments[],
	JSValueRef *exception) {
		g_message("battery_capacity_cb");
		return proxy_double_value(context, "Capacity", argumentCount);
}

static JSValueRef battery_percentage_cb(JSContextRef context, 
	JSObjectRef function, 
	JSObjectRef thisObject,
	size_t argumentCount,
	const JSValueRef arguments[],
	JSValueRef *exception) {
		g_message("battery_percentage_cb");
		return proxy_double_value(context, "Percentage", argumentCount);
}

static JSValueRef battery_voltage_cb(JSContextRef context,
	JSObjectRef function,
    JSObjectRef thisObject,
    size_t argumentCount,
    const JSValueRef arguments[],
    JSValueRef *exception) {
		g_message("battery_voltage_cb");
	return proxy_double_value(context, "Voltage", argumentCount);
}

static JSValueRef battery_update_time_cb(JSContextRef context,
	JSObjectRef function,
    JSObjectRef thisObject,
    size_t argumentCount,
    const JSValueRef arguments[],
    JSValueRef *exception) {
		g_message("battery_update_time_cb");
		return proxy_uint64_value(context, "UpdateTime", argumentCount);
}

static JSValueRef battery_power_supply_cb(JSContextRef context,
    JSObjectRef function,
    JSObjectRef thisObject,
    size_t argumentCount,
    const JSValueRef arguments[],
    JSValueRef *exception) {
		g_message("battery_power_supply_cb");
		return proxy_boolean_value(context, "PowerSupply", argumentCount);
}

static const JSStaticFunction battery_staticfuncs[] = {
	{"capacity", battery_capacity_cb, kJSPropertyAttributeReadOnly},
	{"percentage", battery_percentage_cb, kJSPropertyAttributeReadOnly},
	{"voltage", battery_voltage_cb, kJSPropertyAttributeReadOnly},
	{"updateTime", battery_update_time_cb, kJSPropertyAttributeReadOnly},
	{"powerSupply", battery_power_supply_cb, kJSPropertyAttributeReadOnly},
	{NULL, NULL, 0}	 			
};

/**
* System battery definition.
*/
JSClassDefinition system_battery_def =
{
    0,                     // version
    kJSClassAttributeNone, // attributes
    "Battery",             // className
    NULL,           	   // parentClass
    NULL,                  // staticValues
    battery_staticfuncs,   // staticFunctions
    system_battery_init_cb,       // initialize
    system_battery_destroy_cb,    // finalize
    NULL,                  // hasProperty
    NULL,                  // getProperty
    NULL,                  // setProperty
    NULL,                  // deleteProperty
    NULL,                  // getPropertyNames
    NULL,                  // callAsFunction
    NULL,                  // callAsConstructor
    NULL,                  // hasInstance  
    NULL                   // convertToType
};

