#ifndef _MJAUTV_JSEXT_CUSTOMOBJECT_H_
#define _MJAUTV_JSEXT_CUSTOMOBJECT_H_

#include <JavaScriptCore/JavaScript.h>

/**
* Class init callback.
**/
void class_init_cb(JSContextRef ctx, JSObjectRef object);

/**
* class finalize callback.
*/
void class_finalize_cb(JSObjectRef object);

JSObjectRef class_constructor_cb(JSContextRef ctx,
	JSObjectRef constructor,
	size_t argumentCount,
	const JSValueRef arguments[],
	JSValueRef *exception);


/**
* class definition.
*/
extern const JSClassDefinition class_def;

#endif